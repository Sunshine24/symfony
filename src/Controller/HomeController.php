<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class HomeController extends Controller
{
    /**
     * @Route("/bonjour/{prenom}", name="bonjour")
     * @Route("/bonjour", name="bonjour_simple")
     * @Route("/bonjour/{prenom}{age}", name="bonjour_complet")
     */
    public function bonjour($prenom="", $age=0){
        return new Response("bonjour " . $prenom . " who is " . $age . " yrs old" );
    }

    /**
     * @Route("/home", name="homepage")
     */
    public function home()
    {
        $firstname = ["Michael" => 25, "Rachel" => 18, "Benjamin" => 27];
        return $this->render(
            'home.html.twig',
            [
                'Hello' => "Hello, How are you?",
                'age' => 14,
                'table' => $firstname
            ]
        );
    }
}
